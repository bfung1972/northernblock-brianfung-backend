import express from 'express';
import validate from 'express-validation';
import paramValidation from '../config/param-validation';
import dogCtrl from '../controllers/dog.controller';

const router = express.Router();

router.route('/')
  /** GET /api/dogs - Get list of dogs */
  .get(dogCtrl.list)

  /** POST /api/dogs - Create new dog */
  .post(validate(paramValidation.createDog), dogCtrl.create);

router.route('/:dogId')
  /** GET /api/dogs/:dogId - Get dog */
  .get(dogCtrl.get)

router.route('/:dogId/image')
  /** GET /api/dogs/:dogId/image - Get a dog's image */
  .get(dogCtrl.getImage)



export default router;