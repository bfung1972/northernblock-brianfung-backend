import Dog from '../models/dog.model';


function get(req, res) {
  
  Dog.get(req.params.dogId)
  .then(
    (dog) => res.json(dog)
  ).catch(
    (err) => res.json(err)
  );
  

  
}

function create(req,res) {
  
  const dog = new Dog({
    name: req.body.name,
    breed: req.body.breed,
    sex: req.body.sex,
    weight: req.body.weight,
    location: req.body.location

  });
  return dog.save()
  .then(savedDog => {    
    res.json(savedDog);
  });
}


function list(params) {
  const { limit = 50, skip = 0 } = params;
  return Dog.list({ limit, skip })
}

function getImage(req, res){
  console.log(req.params);
  Dog.get(req.params.dogId)
  .then(
    (dog) => {
      let breed = dog.breed;
      console.log(breed);
      Dog.getImage(breed)
      .then(
        (result) =>{
          console.log("IMAGE");
          //console.log(result);
          return res.json(JSON.parse(result));
        }
      );
      
    }
  ).catch(
    (err) => res.json(err)
  );
  return res; 
}

export default { get, create, list, getImage };
