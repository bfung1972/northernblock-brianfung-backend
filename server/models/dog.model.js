import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { getLeadingCommentRanges } from 'typescript';


/**
 * Dog Schema
 *  name, breed, sex, weight, location
 */
const DogSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  breed: {
    type: String
  },
  sex: {
    type: String
  },
  weight: {
    type: Number
  },
  location: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
* Add your
* - pre-save hooks
* - validations
* - virtuals
*/

/**
 * Methods
 */
DogSchema.method({
});

/**
 * Statics
 */
DogSchema.statics = {
  /**
   * Get Dog
   * @param {ObjectId} id - The objectId of Dog.
   * @returns {Promise<Dog, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((result) => {

        if (result) {
          return Promise.resolve(result);
        }
        const err = new APIError('No such dog exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      }
      )
      .catch((e) => {
        const err = new APIError('No such dog exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List Dogs in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of Dogs to be skipped.
   * @param {number} limit - Limit number of Dogs to be returned.
   * @returns {Promise<Dog[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  },

  getImage(breed) {
  
    return new Promise((resolve,reject)=> {
      let https = require('https');
      let host = "https://dog.ceo";
      let path = "/api/breed/" + breed.toLowerCase() + "/images/random";
        
      https.get(host + path, (resp) => {
        let data = '';
  
        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
          data += chunk;
        });
  
        // The whole response has been received. Print out the result.
        resp.on('end', () => {
          console.log(JSON.parse(data));
          resolve(data);
        });
  
      }).on("error", (e) => {
        const err = new APIError('Dog Image is not found!', httpStatus.NOT_FOUND);
        reject(err);
      });
  
  
    });




  }

};


/**
 * @typedef Dog
 */
export default mongoose.model('Dog', DogSchema);
